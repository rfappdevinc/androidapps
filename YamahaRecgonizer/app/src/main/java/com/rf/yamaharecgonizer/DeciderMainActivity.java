package com.rf.yamaharecgonizer;

import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;


public class DeciderMainActivity extends AppCompatActivity {

    private final String TAG = this.getClass().getName();
    private boolean logged;
    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_decider_main);

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                entrarApp();
            }
        }, 3500);


    }

    private void entrarApp() {
        finish();
        startActivity(new Intent(this, MainActivity.class));
    }

}

