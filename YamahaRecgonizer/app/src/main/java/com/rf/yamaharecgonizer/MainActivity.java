package com.rf.yamaharecgonizer;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private final String TAG = this.getClass().getName();
    private ImageView mImageView;
    private ImageButton mImageButton;
    private TextView mTextView;
    private ProgressBar mProgressBar;

    private int PICK_IMAGE;
    private EditText mEditText;
    private Bitmap image;
    private Context context;
    private File fl;
    private File imageFile;

    private static float mRelativeFaceSize = 0.2f;
    private static int mAbsoluteFaceSize = 0;
    private static final int CV_HAAR_SCALE_IMAGE = 2;

    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1;
    private Mat imageMat;
    private Bitmap afterImg;

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    Log.i("OpenCV", "OpenCV loaded successfully");
                    imageMat = new Mat();
                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mImageView = (ImageView) findViewById(R.id.imageViewShow);
        mImageButton = (ImageButton) findViewById(R.id.imageButtonImportImg);
        mTextView = (TextView) findViewById(R.id.textViewResultVerifyFace);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBarLoadRecFace);

        mProgressBar.setVisibility(View.GONE);

        mImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTextView.setText(" ");
                mImageView.setImageResource(R.mipmap.ic_bgimg);
                createPermissions();
                selectImage();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            Log.d("OpenCV", "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
        } else {
            Log.d("OpenCV", "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }


    private void selectImage() {
        Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
        getIntent.setType("image/*");
        Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickIntent.setType("image/*");
        Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{pickIntent});
        startActivityForResult(chooserIntent, PICK_IMAGE);
    }

    private void createPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_CONTACTS);
                return;
            }
        }
    }

    @Override
    protected void onActivityResult(final int requestCode, int resultCode, Intent data) {
/*
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            if(imageFile.exists()){
                image = decodeFile(imageFile);
                mImageView.setImageBitmap(image);
                mImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            }
        }
*/
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);

            cursor.close();
            image = BitmapFactory.decodeFile(picturePath);
            mImageView.setImageBitmap(image);
            new AsyncTask<Void, Void, Void>(){
                boolean returnSearchFace;
                @Override
                protected Void doInBackground(Void... voids) {
                    try {
                        Thread.sleep(3500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    returnSearchFace = searchFace();
                    return null;
                }

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    mProgressBar.setVisibility(View.VISIBLE);
                    mImageButton.setEnabled(false);
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    mProgressBar.setVisibility(View.GONE);
                    mImageButton.setEnabled(true);
                    if(returnSearchFace) {
                        mImageView.setImageBitmap(afterImg);
                        mTextView.setText("Rosto (s) encontrado (s)!");
                        mTextView.setTextColor(Color.GREEN);
                    }else{
                        mTextView.setText("Rosto (s) não encontrado (s)!");
                        mTextView.setTextColor(Color.RED);
                    }
                }
            }.execute();




        }

    }

    private boolean searchFace() {
        if (image.getWidth() > 1 && image.getHeight() > 1) {
            Mat img_in = new Mat();
            Bitmap bmp32 = image.copy(Bitmap.Config.ARGB_8888, true);
            Utils.bitmapToMat(bmp32, img_in);

            try {
                InputStream is = getResources().openRawResource(R.raw.lbpcascade_frontalface);
                File cascadeDir = getDir("cascade", Context.MODE_PRIVATE);
                File mCascadeFile = new File(cascadeDir,
                        "lbpcascade_frontalface.xml");
                FileOutputStream os = null;
                os = new FileOutputStream(mCascadeFile);

                byte[] buffer = new byte[4096];
                int bytesRead;
                while ((bytesRead = is.read(buffer)) != -1) {
                    os.write(buffer, 0, bytesRead);
                }
                is.close();
                os.close();

                CascadeClassifier cascadeClassifier = new CascadeClassifier(mCascadeFile.getAbsolutePath());
                MatOfRect faces = new MatOfRect();
                if (cascadeClassifier.empty()) {
                    Log.e(TAG, "searchFace: cascadeClassifier.empty()= true" );
                } else {
                    cascadeClassifier.detectMultiScale(img_in, faces, 1.1, 10, CV_HAAR_SCALE_IMAGE,
                            new Size(mAbsoluteFaceSize, mAbsoluteFaceSize), new Size());

                    List<Rect> facesArray = faces.toList();
                    if (facesArray.isEmpty()) {
                        return false;
                    } else {
                        Log.e(TAG, "searchFace: facesArray.size: "+facesArray.size());
                        for (Rect face : facesArray) {
                            Imgproc.rectangle(img_in, face.tl(), face.br(), new Scalar(0, 255, 0), 3);
                        }
                        afterImg = image.copy(Bitmap.Config.ARGB_8888, true);
                        Utils.matToBitmap(img_in, afterImg);
                        return true;
                    }
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }
}
